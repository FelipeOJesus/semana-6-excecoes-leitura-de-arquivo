
public class LeituraArquivoException extends Exception {
	
	private String erro;

	public LeituraArquivoException(String message) {
		super(message);
		this.erro = message;
	}		
	
	public String getErro() {
		return erro;
	}
}

import java.io.File;

public class Principal {

	public static void main(String[] args) throws LeituraArquivoException {
		ProcessadorArquivo processadorArquivo = new ProcessadorArquivo();
		String nomeDoArquivo = System.getProperty("user.dir") + "\\arquivos\\leituraArquivo.txt";
		
		System.out.println(processadorArquivo.processar(nomeDoArquivo));

	}

}

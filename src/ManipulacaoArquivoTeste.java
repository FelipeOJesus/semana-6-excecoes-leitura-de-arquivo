import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ManipulacaoArquivoTeste {
	
	ProcessadorArquivo processadorArquivo;
	
	@Before
	public void inicializador() {
		processadorArquivo = new ProcessadorArquivo();
		
	}

	@Test(expected = LeituraArquivoException.class)
	public void arquivoVazio() throws LeituraArquivoException {
		String nomeDoArquivo = System.getProperty("user.dir") + "\\arquivos\\ArquivoVazio.txt";
		processadorArquivo.processar(nomeDoArquivo);
		
	}
	
	@Test(expected = LeituraArquivoException.class)
	public void erroAbrirArquivo() throws LeituraArquivoException{
		String arquivo = "AAA.txt";
		processadorArquivo.processar(arquivo);
	}
	
	@Test(expected = LeituraArquivoException.class)
	public void formatoDeLinhaInvalida() throws LeituraArquivoException{
		String nomeDoArquivo = System.getProperty("user.dir") + "\\arquivos\\LeituraArquivoFormatoInvalido.txt";
		processadorArquivo.processar(nomeDoArquivo);
	}
	
	@Test
	public void arquivoVazioInformacao(){
		String nomeDoArquivo = System.getProperty("user.dir") + "\\arquivos\\ArquivoVazio.txt";
		try {
			processadorArquivo.processar(nomeDoArquivo);
			fail();
		} catch (LeituraArquivoException e) {
			assertEquals("Arquivo vazio", e.getErro());
		}
		
	}
	
	@Test
	public void erroAbrirArquivoInformacao(){
		String arquivo = "AAA.txt";
		try {
			processadorArquivo.processar(arquivo);
			fail();
		} catch (LeituraArquivoException e) {
			
			assertEquals("Erro ao abrir o arquivo " + arquivo + " (O sistema n�o pode encontrar o arquivo especificado)", e.getErro());
		}
	}
	
	@Test
	public void formatoDeLinhaInvalidaInformacao() {
		String nomeDoArquivo = System.getProperty("user.dir") + "\\arquivos\\LeituraArquivoFormatoInvalido.txt";
		try {
			processadorArquivo.processar(nomeDoArquivo);
			fail();
		} catch (LeituraArquivoException e) {
			assertEquals("Formato de arquivo inv�lido", e.getErro());
		}
	}
	
	

}

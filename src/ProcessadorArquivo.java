import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class ProcessadorArquivo {
	
	public static HashMap<String, String> processar(String nomeDoArquivo) throws LeituraArquivoException{
		HashMap<String, String> informacoes = new HashMap<>();
		
		File arquivo = new File(nomeDoArquivo);
		
		
		
		try {
			Scanner sc = new Scanner(arquivo);
			if(!sc.hasNext())
				throw new LeituraArquivoException("Arquivo vazio");
			while(sc.hasNextLine()) {
				String dados = sc.nextLine().trim();
				if(dados.split("->").length > 2) 
					throw new LeituraArquivoException("Formato de arquivo inv�lido");
				String chave = dados.split("->")[0];
				String valor = dados.split("->")[1];
				
				informacoes.put(chave, valor);
			}
		} catch (FileNotFoundException e) {
			throw new LeituraArquivoException("Erro ao abrir o arquivo " + e.getMessage());
		}
		
		return informacoes;
	}

}
